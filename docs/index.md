# Welcome

This project demonstrates the usage of various frameworks and techniques for the development 
of a Microservice application. It is meant as an example for the Mobile Computing 2 course 
at University of Applied Sciences Erfurt. :thumbsup:

Check out the different branches:

- [Branch 1](https://gitlab.com/steffen.avemarg/quarkus-traefik-docker-compose/-/tree/version_01_quarkus): Simple Setup with Quarkus, Postgres DB, Traefik and Docker Compose 
- [Branch 2](https://gitlab.com/steffen.avemarg/quarkus-traefik-docker-compose/-/tree/version_02_resilience): Second, unreliable Quarkus service we need to cope with
- [Branch 3](https://gitlab.com/steffen.avemarg/quarkus-traefik-docker-compose/-/tree/version_03_tracing_monitoring): Introduced Tracing and Monitoring of our service
- [Branch 4](https://gitlab.com/steffen.avemarg/quarkus-traefik-docker-compose/-/tree/version_04_security): Introduced Security Mechanisms
- [Branch 5](https://gitlab.com/steffen.avemarg/quarkus-traefik-docker-compose/-/tree/version_05_messaging): Introduced Reactive Messaging and Apache Kafka