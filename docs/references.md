## [Basics (Branch 1)](basics.md)
- Traefik: <https://traefik.io>
- Docker Compose: <https://docs.docker.com/compose/>
- Docker Compose file: <https://docs.docker.com/compose/compose-file/>
- Quarkus: <https://quarkus.io>
- Quarkus Database Access with Panache: <https://quarkus.io/guides/hibernate-orm-panache>
- Quarkus Database Access with Panache & Kotlin: <https://quarkus.io/guides/hibernate-orm-panache-kotlin>
- Maven Docker Image: <https://hub.docker.com/_/maven>
- Postgres Docker Image: <https://hub.docker.com/_/postgres>

## [Resilience (Branch 2)](resilience.md)
- Quarkus Fault Tolerance Guide: <https://quarkus.io/guides/microprofile-fault-tolerance>
- MicroProfile Fault Tolerance Spec: <https://github.com/eclipse/microprofile-fault-tolerance>
- Examples on MicroProfile Fault Tolerance: <https://rieckpil.de/whatis-eclipse-microprofile-fault-tolerance/>
- Circuit Breaker Pattern: <https://microservices.io/patterns/reliability/circuit-breaker.html>
- Quarkus REST Client Guide: <https://quarkus.io/guides/rest-client>

## [Tracing (Branch 3)](tracing.md)
- Quarkus - Using Open Tracing: <https://quarkus.io/guides/opentracing>
- MicroProfile OpenTracing with Jaeger and Quarkus: <https://www.youtube.com/watch?v=iqHcTmqj-mo>
- Prometheus: <https://prometheus.io>
- Quarkus - MicroProfile Metrics: <https://quarkus.io/guides/microprofile-metrics>
- Grafana: <https://grafana.com>
- Sentry.io: <https://sentry.io>

## [Security (Branch 4)](security.md)
- Quarkus Basic Auth with Property Files: <https://quarkus.io/guides/security-properties>
- Quarkus Basic Auth with Database & JPA: <https://quarkus.io/guides/security-jpa>
- Short Tutorial for JWT and Quarkus: <https://ard333.medium.com/authentication-and-authorization-using-jwt-on-quarkus-aca1f844996a>
- Quarkus Datasources: <https://quarkus.io/guides/datasource>

## [Messaging (Branch 5)](messaging.md)
- SmallRye Reactive Messaging: <https://smallrye.io/smallrye-reactive-messaging/smallrye-reactive-messaging/2.7/index.html>
- SmallRye Mutiny - Event-Driven Reactive Programming Library for Java: <https://smallrye.io/smallrye-mutiny/>
- Quarkus Guide: Using Apache Kafka With Reactive Messaging: <https://quarkus.io/guides/kafka>
- Kafka Connector Documentation: <https://smallrye.io/smallrye-reactive-messaging/smallrye-reactive-messaging/2.2/kafka/kafka.html>
- Apache Kafka Quickstart: <https://kafka.apache.org/quickstart+>
- kafkacat - Apache Kafka Tool: <https://github.com/edenhill/kafkacat>
- Combining Apache Kafka and the Rest client: <https://quarkus.io/blog/kafka-rest-client/>
- Bridging Imperative and Reactive: <https://quarkus.io/blog/reactive-messaging-emitter/>
- Quarkus and Apache Kafka Streams: <https://quarkus.io/guides/kafka-streams>
- Java Faker: <https://github.com/DiUS/java-faker>